﻿#include <iostream>
#include <string>

int main()
{
    std::string name;
    name = "Ivan Ivanov";

    //I make two decisions simple and beauti
    std::cout << name  << " " << name.length() << " " 
        << name[0] << " " << name[name.length() - 1] << "\n";

    std::cout << "Name" << " " << name << "\n";
    std::cout << "Name length" << " " << name.length() << "\n";
    std::cout << "First symbol " << " " << name[0] << "\n";
    std::cout << "Second symbol" << " " << name[name.length() - 1] << "\n";
    return 0;
} 